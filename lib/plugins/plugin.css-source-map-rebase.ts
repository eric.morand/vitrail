import {Rebaser} from "css-source-map-rebase";
import type {RebaseHandler} from "css-source-map-rebase";
import {Plugin, VitrailContextResource} from "../lib";

export const createCssSourceMapRebasePlugin = (
    rebase: RebaseHandler = (_source, _resolvedPath, done) => done()
): Plugin => {
    return (context, next) => {
        const {resources} = context;

        const resourceHandlers: Array<Promise<VitrailContextResource>> = [];

        for (const resource of resources) {
            const {data, dependencies, name, sourceMap} = resource;

            if (data && sourceMap) {
                const rebaser = new Rebaser({
                    map: Buffer.from(JSON.stringify(sourceMap)),
                    rebase
                });

                // rebaser.on('rebase', (_rebasedPath, resolvedPath) => {
                //     runtimeDependencies.push(resolvedPath);
                // });

                resourceHandlers.push(rebaser.rebase(data)
                    .then((result) => {
                        const resource: VitrailContextResource = {
                            data: result.css,
                            dependencies,
                            name,
                            sourceMap: JSON.parse(result.map.toString())
                        };

                        return resource;
                    })
                );
            }
        }

        return Promise.all(resourceHandlers)
            .then((resources) => {
                return next({
                    resources
                });
            });
    };
};