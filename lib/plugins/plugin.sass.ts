import type {Exception, Options, CompileResult} from "sass";
import {compile, compileString} from "sass";
import {dirname} from "path";
import {Plugin, VitrailContext, VitrailContextResource} from "../lib";

export const createSassPlugin = (
    options?: Options<"sync">
): Plugin => {
    return (context, next) => {
        const {resources} = context;

        const outputContext: VitrailContext = {
            resources: []
        }

        for (const resource of resources) {
            const {name, data} = resource;
            
            try {
                let result: CompileResult;

                const actualOptions = options || {};

                actualOptions.sourceMap = true;
                actualOptions.sourceMapIncludeSources = true;

                if (data) {
                    actualOptions.loadPaths = actualOptions.loadPaths || [];
                    actualOptions.loadPaths.push(dirname(name));

                    result = compileString(data.toString(), actualOptions);
                }
                else {
                    result = compile(name, actualOptions);
                }

                const {sourceMap, loadedUrls} = result;
                const dependencies = loadedUrls.map((loadedUrl) => loadedUrl.pathname);
                
                outputContext.resources.push({
                    dependencies,
                    data: Buffer.from(result.css),
                    name,
                    sourceMap
                });
            } catch (error: any) {
                const exception = error as Exception;

                console.error(exception);

                if (exception.span.url) {
                    outputContext.resources.push({
                        name: exception.span.url.pathname,
                        dependencies: [
                            exception.span.url.pathname
                        ]
                    });
                }

                outputContext.error = error;
            }
        }
        
        return next(outputContext);
    }
};