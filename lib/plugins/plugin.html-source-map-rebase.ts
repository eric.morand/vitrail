import {createRebaser} from "html-source-map-rebase";
import type {RebaseHandler} from "html-source-map-rebase";
import {Plugin, VitrailContextResource} from "../lib";

export const createHtmlSourceMapRebasePlugin = (
    rebase: RebaseHandler = (_source, _resolvedPath, done) => done()
): Plugin => {
    return (context, next) => {
        const {resources} = context;
        
        const resourceHandlers: Array<Promise<VitrailContextResource>> = [];

        for (const resource of resources) {
            const {data, dependencies, name, sourceMap} = resource;

            if (data && sourceMap) {
                const rebaser = createRebaser(Buffer.from(JSON.stringify(sourceMap)), {
                    rebase
                });

                // rebaser.on('rebase', (_rebasedPath, resolvedPath) => {
                //     runtimeDependencies.push(resolvedPath);
                // });

                resourceHandlers.push(rebaser.rebase(data)
                    .then(({data, map}) => {
                        const resource: VitrailContextResource = {
                            dependencies,
                            data,
                            name,
                            sourceMap: JSON.parse(map.toString())
                        };

                        return resource;
                    })
                );
            }
        }

        return Promise.all(resourceHandlers)
            .then((resources) => {
                return next({
                    resources
                });
            });
    };
};