import {createSourceMapRuntime, TwingEnvironment} from "twing";
import {Plugin, VitrailContextResource} from "../lib";

export const createTwigPlugin = (
    environment: TwingEnvironment
): Plugin => {
    return (context, next) => {
        const {resources} = context;
        
        return Promise.all(resources.map((resource) => {
            const {name, dependencies} = resource;
            const loadedTemplates: Array<[name: string, from: string | null]> = [];
            
            dependencies.push(name);

            environment.on("load", (name, from) => {
                loadedTemplates.push([name, from]);
            });

            const sourceMapRuntime = createSourceMapRuntime()

            return environment.loadTemplate(name)
                .then((template) => {
                    return template.render(resource, {
                        sourceMapRuntime
                    });
                })
                .then((output) => {
                    const {sourceMap} = sourceMapRuntime;

                    return Promise.all(loadedTemplates.map(([name, from]) => {
                        return environment.loader.resolve(name, from || null)
                            .then((resolvedTemplatePath) => {
                                return resolvedTemplatePath || name;
                            });
                    })).then((resolvedTemplates) => {
                        dependencies.push(...resolvedTemplates);
                        
                        const resource: VitrailContextResource = {
                            dependencies,
                            data: Buffer.from(output),
                            name,
                            sourceMap
                        }

                        return resource;
                    });
                });
        })).then((resources) => {
            return next({
                resources
            });
        });
    };
};