import {readFileSync} from "fs";
import {Plugin, VitrailContext} from "../lib";

export const createCopyPlugin = (): Plugin => {
    return (context, next) => {
        const {resources} = context;
        const outputContext: VitrailContext = {
            resources: []
        };

        for (let {name, data} of resources) {
            if (data === undefined) {
                data = readFileSync(name);
            }

            outputContext.resources.push({
                data,
                dependencies: [name],
                name
            });
        }

        return next(outputContext);
    };
};