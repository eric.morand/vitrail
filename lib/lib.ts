import {Middleware} from "@arabesque/core";
import {Context} from "@stromboli/core/types/lib/context";
import {Component} from "@stromboli/core";
import {RawSourceMap} from "source-map-js";

export type VitrailResponse = {
    artifacts: Context["response"]["artifacts"];
    buildDependencies: Array<string>;
    runtimeDependencies: Array<string>;
    error?: Error;
};

export type VitrailContextResource = {
    data?: Buffer;
    dependencies: Array<string>;
    name: string;
    sourceMap?: RawSourceMap;
};

export type VitrailContext = {
    error?: Error;
    resources: Array<VitrailContextResource>;
};

export type Plugin = Middleware<VitrailContext>;

export type Resource = {
    name: string;
    dependencies: Array<{
        type: "build" | "runtime";
        resource: Resource;
    }>;
};