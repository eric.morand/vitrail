import {createANDMiddleware} from "@arabesque/logic-middlewares";
import {createTwigPlugin} from "./lib/plugins/plugin.twig";
import {createEnvironment, createFilesystemLoader} from "twing";
import * as fs from "fs";
import {outputFileSync, emptyDirSync} from "fs-extra";
import {dirname, relative, join} from "path";
import {watch} from "chokidar";
import BrowserSync from "browser-sync";
import {createHtmlSourceMapRebasePlugin} from "./lib/plugins/plugin.html-source-map-rebase";
import {VitrailContext} from "./lib/lib";
import {createSassPlugin} from "./lib/plugins/plugin.sass";
import {createCssSourceMapRebasePlugin} from "./lib/plugins/plugin.css-source-map-rebase";
import {createCopyPlugin} from "./lib/plugins/plugin.copy";

type Route = {
    pattern: RegExp;
    handler: (
        context: VitrailContext,
        schedule: (name: string) => void
    ) => Promise<VitrailContext>;
    output: (name: string) => string;
};

const getMatchingRoute = (name: string): Route | null => {
    return routes.find((route) => {
        return route.pattern.test(name);
    }) || null;
};

const routes: Array<Route> = [
    {
        pattern: /\.twig$/,
        handler: (context, schedule) => {
            const middleware = createANDMiddleware(
                createTwigPlugin(
                    createEnvironment(
                        createFilesystemLoader(fs)
                    )
                ),
                createHtmlSourceMapRebasePlugin((source, resolvedPath, done) => {
                    let rewrittenPath = relative(dirname(source), resolvedPath);

                    const route = getMatchingRoute(resolvedPath);

                    if (route) {
                        rewrittenPath = route.output(rewrittenPath);
                    }

                    schedule(resolvedPath);

                    done(rewrittenPath);
                })
            );

            return middleware(context, (context) => Promise.resolve(context));
        },
        output: (name) => {
            return name.replace(/\.twig$/, '');
        }
    },
    {
        pattern: /\.scss$/,
        handler: (context, schedule) => {
            const render = createANDMiddleware(
                createSassPlugin(),
                createCssSourceMapRebasePlugin((source, resolvedPath, done) => {
                    let rewrittenPath = relative(dirname(source), resolvedPath);

                    const route = getMatchingRoute(resolvedPath);

                    if (route) {
                        rewrittenPath = route.output(rewrittenPath);
                    }

                    schedule(resolvedPath);

                    done(rewrittenPath);
                })
            );

            return render(context, (context) => Promise.resolve(context));
        },
        output: (name) => {
            return name.replace(/\.scss$/, '.css');
        }
    },
    {
        pattern: /.*/,
        handler: (context) => {
            const copy = createCopyPlugin();

            return copy(context, (context) => Promise.resolve(context));
        },
        output: (name) => name
    }
];

// start
type StackEntry = {
    name: string;
    parent?: string;
    output?: string;
};

const handle = async (name: string, parent?: string): Promise<void> => {
    console.log('================');
    console.log('WE HANDLE RESOURCE', name);
    console.log('================');

    const stack: Array<StackEntry> = [];
    const done: Array<StackEntry> = [];

    const schedule = (name: string) => {
        console.log('Scheduling', name);

        const stackEntry = done.find((stackEntry) => {
            return stackEntry.name === name;
        }) || stack.find((stackEntry) => {
            return stackEntry.name === name;
        });

        if (!stackEntry) {
            console.log('', name, 'scheduled successfully');

            stack.push({
                name
            });
        }
        else {
            console.log('', name, 'is already scheduled');
        }
    };

    const _handle = (name: string, parent?: string): Promise<VitrailContext | null> => {
        console.log('----------------')
        console.log('WE _HANDLE RESOURCE', name);
        console.log('----------------')

        const route = getMatchingRoute(name);

        const context: VitrailContext = {
            resources: [{
                name,
                dependencies: []
            }],
        };

        if (route) {
            return route.handler(context, schedule)
                .then((context) => {
                    const {resources} = context;

                    for (const resource of resources) {
                        // normalize names
                        resource.name = relative('.', resource.name); // . is organically the current context, that is the entry point

                        // normalize dependencies
                        resource.dependencies = resource.dependencies?.map((dependency) => relative('.', dependency)) || [];

                        const {dependencies} = resource;

                        const watchFiles = () => {
                            const watcher = watch(dependencies);

                            watcher.on("change", () => {
                                // one of the dependency of the resource did change: rebuild the resource
                                return watcher.close()
                                    .then(() => {
                                        return handle(resource.name, parent);
                                    });
                            });
                        };

                        watchFiles();
                    }

                    return context;
                });
        }

        return Promise.resolve(null);
    };

    schedule(name);

    const context: VitrailContext = {
        resources: [{
            name,
            dependencies: []
        }]
    };

    while (stack.length > 0) {
        const stackEntry = stack.pop()!;
        const stackEntryContext = await _handle(stackEntry.name, parent);

        if (stackEntryContext) {
            const {resources} = stackEntryContext;

            context.resources.push(...resources);
        }

        done.push(stackEntry);
    }

    for (const resource of context.resources) {
        let fileName = resource.name;

        const route = getMatchingRoute(fileName);

        if (route) {
            fileName = route.output(fileName);
        }

        if (resource.data) {
            outputFileSync(join('www', fileName), resource.data);
        }
    }

    //console.log('RELOAD', name);

    //browserSyncInstance.reload(name);
};

// entry point
emptyDirSync('www');

console.log(`╔══════════════════════════════════╦═════════╦════════════════════════╦════════════════╗`)

const browserSyncInstance = BrowserSync.create();

handle('test/index.html.twig')
    .then(() => {
        browserSyncInstance.init({
            server: 'www/test',
            open: false,
            watch: true
        });
    });
