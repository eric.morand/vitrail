import {createANDMiddleware} from "@arabesque/logic-middlewares";
import {createTwigPlugin} from "./lib/plugins/plugin.twig";
import {createArrayLoader, createEnvironment, createFilesystemLoader, createFunction} from "twing";
import * as fs from "fs";
import {outputFileSync, emptyDirSync} from "fs-extra";
import {dirname, relative, join} from "path";
import {watch} from "chokidar";
import BrowserSync from "browser-sync";
import {createHtmlSourceMapRebasePlugin} from "./lib/plugins/plugin.html-source-map-rebase";
import {Component} from "@stromboli/core";
import {Resource, VitrailContext} from "./lib/lib";
import {createSassPlugin} from "./lib/plugins/plugin.sass";
import {createCssSourceMapRebasePlugin} from "./lib/plugins/plugin.css-source-map-rebase";
import {createCopyPlugin} from "./lib/plugins/plugin.copy";

let log = (content: string) => {
};

const handledDependencies: Array<{
    name: string;
    isFresh: boolean;
}> = [];

type Route = {
    pattern: RegExp;
    handler: (component: Component) => Promise<VitrailContext>; // todo: Promise<Response> is enough
    output: (name: string) => string;
};

const getMatchingRoute = (name: string): Route | null => {
    return routes.find((route) => {
        return route.pattern.test(name);
    }) || null;
};

const routes: Array<Route> = [
    {
        pattern: /\.twig$/,
        handler: (component) => {
            const response: VitrailContext["response"] = {
                artifacts: [],
                buildDependencies: [],
                runtimeDependencies: []
            };

            const middleware = createANDMiddleware(
                createTwigPlugin(
                    createEnvironment(
                        createFilesystemLoader(fs)
                    )
                ),
                createHtmlSourceMapRebasePlugin((source, resolvedPath, done) => {
                    let rewrittenPath = relative(dirname(source), resolvedPath);

                    const route = getMatchingRoute(resolvedPath);

                    if (route) {
                        rewrittenPath = route.output(rewrittenPath);
                    }

                    done(rewrittenPath);
                })
            );

            return middleware({
                component,
                response
            }, (context) => Promise.resolve(context));
        },
        output: (name) => {
            return name.replace(/\.twig$/, '');
        }
    },
    {
        pattern: /\.scss$/,
        handler: (component) => {
            const response: VitrailContext["response"] = {
                artifacts: [],
                buildDependencies: [],
                runtimeDependencies: []
            };

            const render = createANDMiddleware(
                createSassPlugin(),
                createCssSourceMapRebasePlugin((source, resolvedPath, done) => {
                    let rewrittenPath = relative(dirname(source), resolvedPath);

                    const route = getMatchingRoute(resolvedPath);

                    if (route) {
                        rewrittenPath = route.output(rewrittenPath);
                    }

                    done(rewrittenPath);
                })
            );

            return render({
                component,
                response
            }, (context) => Promise.resolve(context));
        },
        output: (name) => {
            return name.replace(/\.scss$/, '.css');
        }
    },
    {
        pattern: /\.ts$/,
        handler: (component) => {
            const response: VitrailContext["response"] = {
                artifacts: [],
                buildDependencies: [],
                runtimeDependencies: []
            };

            const render = createCopyPlugin();

            return render({
                component,
                response
            }, (context) => Promise.resolve(context));
        },
        output: (name) => {
            return name.replace(/\.ts$/, '.js');
        }
    },
    {
        pattern: /.*/,
        handler: (component) => {
            const copy = createCopyPlugin();

            return copy({
                component,
                response: {
                    artifacts: [],
                    buildDependencies: [],
                    runtimeDependencies: []
                }
            }, (context) => Promise.resolve(context));
        },
        output: (name) => name
    }
];

// start
type StackEntry = {
    resource: Resource;
    parent?: Resource;
    output?: string;
};

const getEntry = (name: string) => handledDependencies.find((entry) => entry.name === name);

const handle = async (resource: Resource, parent?: Resource): Promise<void> => {
    console.log('================')
    console.log('WE HANDLE RESOURCE', resource);
    console.log('================')

    const stack: Array<StackEntry> = [];

    const _handle = (name: string, parent: string | null): Promise<VitrailContext> => {
        console.log('----------------')
        console.log('WE _HANDLE', name);
        console.log('----------------')

        const entry = getEntry(name);

        if (entry) {
            entry.isFresh = true;
        }
        else {
            handledDependencies.push({
                name,
                isFresh: true
            });
        }

        const route = getMatchingRoute(name);

        const component: Component = {
            name,
            path: name
        };

        if (route) {
            return route.handler(component)
                .then((context) => {
                    console.log(context);

                    const {response} = context;

                    // sanitize artifacts
                    if (parent) {
                        for (const artifact of response.artifacts) {
                            artifact.name = relative('.', artifact.name); // . is organically the current context, that is the entry point
                        }
                    }

                    // watch the runtime dependencies
                    // => on change, trigger a build on the dependency and reload the dependency

                    // watch the dependencies
                    const watchFiles = (
                        buildDependencies: Array<string>,
                        runtimeDependencies: Array<string>
                    ) => {
                        const buildDependenciesWatcher = watch(buildDependencies);

                        console.log('WATCHING BUILD DEPS', buildDependencies);

                        buildDependenciesWatcher.on("change", (file) => {
                            console.log('BUILD DEP', file, 'DID CHANGE');

                            return buildDependenciesWatcher.close()
                                .then(() => {
                                    const entry = getEntry(file);

                                    if (entry) {
                                        entry.isFresh = false;
                                    }

                                    return handle(name, parent);
                                });
                        });

                        const runtimeDependenciesWatcher = watch(runtimeDependencies);

                        console.log('WATCHING RUNTIME DEPS', runtimeDependencies);

                        runtimeDependenciesWatcher.on("change", (file) => {
                            console.log('RUNTIME DEP', file, 'DID CHANGE');

                            return runtimeDependenciesWatcher.close()
                                .then(() => {
                                    const entry = getEntry(file);

                                    if (entry) {
                                        entry.isFresh = false;
                                    }

                                    return handle(file, name);
                                });
                        });
                    }

                    const dependencies = [...response.buildDependencies, ...response.runtimeDependencies];

                    watchFiles(
                        [
                            name,
                            ...response.buildDependencies
                        ],
                        response.runtimeDependencies);

                    for (const dependency of dependencies) {
                        console.log('  WE CHALLENGE DEP', dependency, 'of', name);

                        const entry = getEntry(dependency);

                        if (!entry || !entry.isFresh) {
                            console.log('   !!!!>', dependency, 'IS NOT FRESH, LET US MOVE ON');

                            const route = getMatchingRoute(dependency);

                            if (route) {
                                stack.push({
                                    name: dependency,
                                    parent: component.name,
                                    output: route.output(dependency)
                                });
                            }
                            else {
                                stack.push({
                                    name: dependency,
                                    parent: component.name,
                                    output: dependency
                                });
                            }
                        }
                        else {
                            console.log('   ', dependency, 'IS STILL FRESH, SO WE BAIL');
                        }
                    }

                    return context;
                });
        }

        return Promise.resolve({
            component,
            response: {
                artifacts: [],
                buildDependencies: [],
                runtimeDependencies: []
            }
        });
    };

    stack.push({
        resource,
        parent
    });

    const {name} = resource;
    
    const context: VitrailContext = {
        component: {
            name,
            path: name
        },
        response: {
            artifacts: [],
            buildDependencies: [],
            runtimeDependencies: []
        }
    };

    while (stack.length > 0) {
        const stackEntry = stack.pop()!;

        const {response} = await _handle(stackEntry.name, stackEntry.parent);

        context.response.artifacts.push(...response.artifacts);
        context.response.buildDependencies.push(...response.buildDependencies);
        context.response.runtimeDependencies.push(...response.runtimeDependencies);
    }

    const {artifacts} = context.response;

    for (const artifact of artifacts) {
        let fileName = artifact.name;

        const route = getMatchingRoute(fileName);

        if (route) {
            fileName = route.output(fileName);
        }

        outputFileSync(join('www', fileName), artifact.data);
    }

    console.log('RELOAD', name);

    browserSyncInstance.reload(name);
};

// entry point
emptyDirSync('www');

console.log(`╔══════════════════════════════════╦═════════╦════════════════════════╦════════════════╗`)

const browserSyncInstance = BrowserSync.create();

handle({
    name: 'test/index.html.twig',
    dependencies: []
}).then(() => {
    browserSyncInstance.init({
        server: 'www/test',
        open: false
    });
});
